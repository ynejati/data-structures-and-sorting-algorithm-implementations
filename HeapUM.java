import java.util.Arrays;

/**
 * File: HeapUM.java
 * Author: Yousuf Nejati
 * Date: September 13th 2015
 * Purpose: HeapUM class creates a heap data structure
 * Will be a MIN HEAP, so that the value of any parent node
 * at any time, is less than or equal to any one of it's
 * children
 **/

public class HeapUM {

    /**
     * Class variables
     */
    char[] heapArray = new char[11];
    private static int heapSize = 1;


    /**
     * Insert node in heap, preforms checks to make sure the value being added is not
     * less than the parent node
     */
    public void insert(Character value) {

        // If the size of the heap is greater than 10,... throw an exception
        if (heapSize > 11) {
            throw new RuntimeException("Heap overflow");
        }
        // If the heap holds only one value, ... add that value
        else if (heapSize == 1) {
            // Just add the value to the first index (1)
            heapArray[1] = value;
        }

        /*
        Else, if the size is anything greater than one,
        add the value to the next index of the array and preform the check.
         */
        else {

            // Add the value to the next index of the array
            heapArray[heapSize] = value;

            // If the size of the heap is odd,do this...
            if (heapSize % 2 != 0) {

                // Subtract one from the heap size and assign that value as 'parent'
                Character parent = heapArray[(heapSize - 1) / 2];

                // Assign the value of the last node (added node) as 'newNode'
                Character newNode = heapArray[heapSize];

                // Compare the value of the added node and its parent
                int comparisonValue = newNode.compareTo(parent);

                // if the comparison value is less than or equal to 0, do this...
                if (comparisonValue <= 0) {

                    // Switch the parent and the child
                    // Parent now holds the value for the added node
                    heapArray[(heapSize - 1) / 2] = newNode;

                    // Child now holds the value of the old parent
                    heapArray[heapSize] = parent;
                }
            } else {

                // Assign the value of the added nodes parent to local variable
                Character parent = heapArray[heapSize / 2];

                // Assign the value of the last node (added node) to a local variable
                Character newNode = heapArray[heapSize];

                // Compare the value of the added node and its parent
                int comparisonValue = newNode.compareTo(parent);

                // if the comparison value is less than or equal to 0, do this...
                if (comparisonValue <= 0) {

                    // Switch the parent and the child
                    // Parent now holds the value for the added node
                    heapArray[heapSize / 2] = newNode;

                    // Child now holds the value of the old parent
                    heapArray[heapSize] = parent;
                }
            }
        }

        heapSize++;
    }

    /**
     * Gets the size of the heap
     * @return
     */
    public int getHeapSize() {
        return heapSize;
    }

    /**
     * Default toString method uses the
     * Arrays toString method to print a proper
     * representation of the values of the array
     */
    @Override public String toString() {
        return Arrays.toString(heapArray);
    }
}




