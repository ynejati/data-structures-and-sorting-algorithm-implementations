import java.util.Arrays;

/**
 * File: QueueUM.java
 * Author: Yousuf Nejati
 * Date: August 30th 2015
 * Purpose: Implement our own queue to store instances of the Gym_Member class, and create
 * custom methods specific to a queue
 */
public class QueueUM<T> {

    /**
     * Class variables
     */
    private int queueSize = 0;
    private Object elements[];
    private int top;
    private int bottom;
    protected int nItems;

    /**
     * Class constructor, accepts an int as a parameter
     *
     * @param number
     */
    public QueueUM(int number) {
        queueSize = number;
        elements = new Object[queueSize];
        top = -1;
        bottom = -1;
        nItems = 0;
    }

    /**
     * Inserts the specified element into this queue if it is possible to do so immediately
     * without violating capacity restrictions, returning true upon success and throwing
     * exception if no space is currently available
     *
     * @param object
     */
    public void put(T object) {
        if (bottom == queueSize - 1) {
            bottom = -1;
            elements[++bottom] = object;
            nItems++;
        }
        elements[++top] = object;
    }

    /**
     * Retrieves and removes the head of this queue
     *
     * @return
     */
    //T get ()
    public T get() {
        T x = (T) elements[++bottom];
        elements[bottom] = null + ", Index " + (bottom + 1) + " is empty! \n";
        if (top == queueSize) {
            top = 0;
            nItems--;
        }
        return x;
    }


    /**
     * Retrieves, but does not remove, the head of this queue, or returns null
     * if this queue is empty
     *
     * @return
     */
    public T peek() {
        return (T) elements[bottom + 1];
    }

    /**
     * String representation of this class
     *
     * @return
     */
    //String toString ()
    @Override public String toString() {
        return Arrays.deepToString(elements);
    }
}
