import com.sun.tools.internal.ws.wsdl.framework.DuplicateEntityException;

import java.util.Scanner;

/**
 * File: BinaryTreeUM.java
 * Author: Yousuf Nejati
 * Date: 09/11/15
 * Purpose: Implement our own Binary Tree with an internal Node class, using at minimum, the required
 * methods taken from the project specifications (and Tree traversal). To be used in with the UI and
 * Object class (Gym Member) created in Homework 1.
 */

// Generic type extends the Comparable class, only objects of similar type can be compared
public class BinaryTreeUM<T extends Comparable> {

    /**
     * Class instance variables
     */
    NodeTreeBin<T> root = null;
    NodeTreeBin<T> currentNode = null;
    NodeTreeBin<T> foundNode = null;


    /**
     * Constructor:
     * Scans a file and creates a binary tree based upon the data contained in
     * the file.
     *
     * @param scanner
     */
    BinaryTreeUM(Scanner scanner) {

        // While the scanner has values create a new node and add it to the binary tree
        while (scanner.hasNextLine()) {

            // Call insert method
            insert((T) scanner.nextLine());
        }
    }

    /**
     * Constructor:
     * Default, no argument, class constructor
     */
    BinaryTreeUM() {
        root = null;
    }


    /**
     * Supplements the 'insertNode' method.
     * Provides an initial check of root before proceeding to insert a node into the binary tree.
     */
    public void insert(T value) {

        // if the root is null, set then newNode as the root
        if (root == null) {
            root = new NodeTreeBin<T>(value);

            // Root is not null, call the 'insertNode' method
        } else {
            insertNode(root, value);
        }
    }


    /**
     * Inserts a node into a binary tree using recursive methods
     * Accepts a node, and generic value
     *
     * @param currentNode
     * @param
     */
    private void insertNode(NodeTreeBin<T> currentNode, T value) {

        // The current value equals the value of the current node
        T curVal = (T) currentNode.getCargo();

        // Compare values and assign integer value to local variable
        int compareResult = curVal.compareTo(value);

        // If values are equal
        if ((currentNode.equals(value))) {
            throw new DuplicateEntityException(null, "");

            // If values are less than 0
        } else if (compareResult < 0) {
            NodeTreeBin<T> rightChild = currentNode.getRightChild();
            if (rightChild == null) {
                currentNode.setRightChild(new NodeTreeBin<T>(value));
            } else {
                insertNode(rightChild, value);
            }
            // If values are greater than 0
        } else {
            NodeTreeBin<T> leftChild = currentNode.getLeftChild();
            if (leftChild == null) {
                currentNode.setLeftChild(new NodeTreeBin<T>(value));
            } else {
                insertNode(leftChild, value);
            }
        }
    }

    /**
     * Supplements the required findElement class, almost identical to method used to
     * insert a node. However, returns node containing desired value
     *
     * @param currentNode
     * @param findThisString
     * @return
     */
    public NodeTreeBin<T> findNode(NodeTreeBin currentNode, String findThisString) {

        // The current value equals the value of the current node
        T curVal = (T) currentNode.getCargo();

        // Compare values and assign integer value to local variable
        int compareResult = curVal.compareTo(findThisString);

        // If values are equal
        if ((currentNode.equals(findThisString))) {
            foundNode = currentNode;

            // If values are less than 0
        } else if (compareResult < 0) {
            NodeTreeBin rightChild = currentNode.getRightChild();
            if (rightChild == null) {
                return null;
            } else {
                findNode(rightChild, findThisString);
            }

            // If values are greater than 0
        } else {
            NodeTreeBin leftChild = currentNode.getLeftChild();
            if (leftChild == null) {
                return null;
            } else {
                findNode(leftChild, findThisString);
            }
        }
        // Return node
        return foundNode;
    }


    /**
     * findElement()
     * Returns the nodes cargo matching the String parameter,
     * and define the meaning of match in the context of the class T
     */
    public NodeTreeBin<T> findElement(String findThisString) {
        NodeTreeBin<T> pointerNode;

        if (root == null) {
            throw new NullPointerException("Empty tree");
        } else {
            pointerNode = findNode(root, findThisString);
        }
        return pointerNode;
    }



    /**
     * Recursive method, supplements 'insertNode' method's logic upon
     * node insertion.
     * Logic is very similar to search, and insert methods.
     *
     * @param newNode
     * @param
     */

    private void insertLeftChild(NodeTreeBin<T> newNode) {

        /*
         After moving left, if the current node is null, set the current node to the new node.
         This is what allows exit of the recursive method
         */
        if (currentNode == null) {
            currentNode = newNode;
        }

        // if the current node does not equal null
        else {

            // Check if the value of the current node is equal to the new node
            if (currentNode.getCargo().equals(newNode.getCargo())) {
                throw new DuplicateEntityException(null, "Duplicate value found");
            }

            // Check if the value is less than the current node
            if (currentNode.getCargo().compareTo(newNode.getCargo()) < 0) {
                // Move to the left of the current node, and set the left child to current node
                currentNode = currentNode.getLeftChild();
                // Preform the same same check on the left child of the current node
                insertLeftChild(newNode);
            }

            // Check if the value is greater than the current node
            if (currentNode.getCargo().compareTo(newNode.getCargo()) > 0) {
                // Move to the right of the current node, and set right child to the current node
                currentNode = currentNode.getRightChild();
                // Call the insert right child method on the new node
                insertRightChild(newNode);
            }
        }
    }


    /**
     * Recursive method, supplements 'insertNode' method's logic upon
     * node insertion.
     * Logic is very similar to search, and insert methods.
     *
     * @param newNode
     */

    private void insertRightChild(NodeTreeBin newNode) {
        /*
         After moving right, if the current node is null, set the current node to the new node.
         This is what allows exit of the recursive method
         */
        if (currentNode == null) {
            currentNode = newNode;
        }

        // if the current node does not equal null
        else {

            // Check if the value of the current node is equal to the new node
            if (currentNode.getCargo().equals(newNode.getCargo())) {
                throw new DuplicateEntityException(null, "Duplicate value found");
            }

            // Check if the value is greater than the current node
            if (currentNode.getCargo().compareTo(newNode.getCargo()) > 0) {
                // Move to the right of the current node, and set right child to the current node
                currentNode = currentNode.getRightChild();
                // Call the insert right child method on the new node
                insertRightChild(newNode);
            }

            // Check if the value is less than the current node
            if (currentNode.getCargo().compareTo(newNode.getCargo()) < 0) {
                // Move to the left of the current node, and set the left child to current node
                currentNode = currentNode.getLeftChild();
                // Preform the same same check on the left child of the current node
                insertLeftChild(newNode);
            }
        }
    }


    /**
     * Get the root of the binary tree
     *
     * @return
     */
    public NodeTreeBin getRoot() {
        return root;
    }


    /**
     * Binary tree class toString method
     *
     * @return
     */
    public String toString() {
        return "Binary Tree: ";
    }


    /**
     * Pre Order Tree traversal, prints the cargo of the nodes
     * in DLR fashion
     * (DLR) Current node, left subtree, right subtree
     *
     * @return
     */
    public String toPreOrderString() {
        return root.toPreOrderString();
    }


    /**
     * Post Order Tree traversal, prints the cargo of the nodes
     * in LRD fashion
     * (LRD) Left subtree, right subtree, current node
     *
     * @return
     */
    public String toPostOrderString() {
        return root.toPostOrderString();
    }


    /**
     * In Order Tree traversal, prints the cargo of the nodes
     * in LDR fashion
     * (LDR) Left subtree, current node, right subtree
     *
     * @return
     */
    public String toInOrderString() {
        return root.toInOrderString();
    }



    // ++++++++++++++++++++++        BEGIN INTERNAL NODE CLASS        ++++++++++++++++++++++++++++++


    /**
     * Internal binary tree node class.
     *
     * @param <T>
     */
    private static class NodeTreeBin<T> {

        /**
         * Instance variables
         */
        private NodeTreeBin<T> leftChild;
        private NodeTreeBin<T> rightChild;
        private T cargo;


        /**
         * Class constructor accepts generic type T
         *
         * @param cargo
         */
        NodeTreeBin(T cargo) {
            this.cargo = cargo;
        }


        /**
         * Traverse in DLR fashion
         * This is the preOrder method, recursively traverses the
         * binary tree in the order of Parent, Left child, Right child
         */
        String toPreOrderString() {
            String myString = toString();
            String leftChildString = leftChild == null ? "" : leftChild.toPreOrderString();
            String rightChildString = rightChild == null ? "" : rightChild.toPreOrderString();

            //Presents the order in which to print strings initialized by the ternary operators
            return myString + leftChildString + rightChildString;
        }


        /**
         * Traverse in LRD fashion
         * postOrder method, recursively traverses the binary tree
         * in the order of Left child, Right child, and Parent
         */
        String toPostOrderString() {
            String myString = toString();
            String leftChildString = leftChild == null ? "" : leftChild.toPostOrderString();
            String rightChildString = rightChild == null ? "" : rightChild.toPostOrderString();

            //Presents the order in which to print strings initialized by the ternary operators
            return leftChildString + rightChildString + myString;
        }


        /**
         * Traverse in LDR fashion
         * inOrder method, recursively traverse the binary tree
         * in th order of Left child, Parent, and Right child
         */
        String toInOrderString() {
            String myString = toString();
            String leftChildString = leftChild == null ? "" : leftChild.toInOrderString();
            String rightChildString = rightChild == null ? "" : rightChild.toInOrderString();

            //Presents the order in which to print strings initialized by the ternary operators
            return leftChildString + myString + rightChildString;
        }


        /**
         * Gets left child
         *
         * @return
         */
        NodeTreeBin getLeftChild() {
            return leftChild;

        }


        /**
         * Gest right child
         *
         * @return
         */
        NodeTreeBin getRightChild() {
            return rightChild;
        }


        /**
         * Gets nodes cargo
         *
         * @return
         */
        public T getCargo() {
            return cargo;
        }


        /**
         * Sets the left child
         *
         * @param leftChild
         */
        public void setLeftChild(NodeTreeBin leftChild) {
            this.leftChild = leftChild;
        }


        /**
         * Sets the right child
         *
         * @param rightChild
         */
        public void setRightChild(NodeTreeBin rightChild) {
            this.rightChild = rightChild;
        }


        /**
         * toString belonging to the node class
         * returns the string representation of the nodes cargo
         *
         * @return
         */
        @Override public String toString() {
            return cargo.toString() + "\n";
        }

        // +++++++++++++++++++++        END INTERNAL NODE CLASS        +++++++++++++++++++++++++++++

    }
}




