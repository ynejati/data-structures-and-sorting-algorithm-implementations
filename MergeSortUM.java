import java.util.Arrays;

/**
 * File: MergeSortUM.java
 * Author: Yousuf Nejati
 * Date: September 22nd 2015
 * Purpose: Implement an ((n)log(n)) sorting algorithm to sort an array of strings using default
 * lexicographical values
 */
public class MergeSortUM {

    /**
     * The first component of the mergeSort process, divides the parent array into two sub arrays
     * (childArrayOne and childArrayTwo), then recursively calls the method again on the two
     * sub arrays. Calls and returns the second component of the mergeSort process, which is the
     * merge method that follows.
     *
     * @param parentArray
     * @return
     */
    public static GymMember[] mergeSort(GymMember[] parentArray) {
        // Local variable gets the length of the parent array
        int arrayLength = parentArray.length;

        // Declare two new sub arrays
        // Holds the first half of the parent array
        GymMember[] childArrayOne;

        // Holds the second half of the parent array
        GymMember[] childArrayTwo;

        // If the array is only one element, no sorting needs to be done, return the parent array
        if (arrayLength == 1) {

            // return the parent array
            return parentArray;

            // If the array is anything greater other than one
        } else {

            // Declare a local variable to represent the median of the the array
            int half;

            // If the size of the array is even, simply find the median by dividing by two
            if (arrayLength % 2 == 0) {

                // Our local variable is now equal to half the length of the parent array
                half = arrayLength / 2;
            }

            /*
             Otherwise, the size of the array is odd, so subtract one from the length of the array
             and then divide by two
             */
            else {

                // Our local variable is now equal to half the length of the parent, minus one
                half = (arrayLength - 1) / 2;
            }

            /*
             Split the parent array into two sub arrays based upon the initializing values
             set above in the previous if-else block. Use the copyOfRange method from the Array
             class to copy the values of the parent array into our two new sub arrays
             */
            childArrayOne = Arrays.copyOfRange(parentArray, 0, half);
            childArrayTwo = Arrays.copyOfRange(parentArray, half, arrayLength);

            // Recursive call, split the array again, the sub array now has it's own sub arrays
            childArrayOne = mergeSort(childArrayOne);

            // Recursive call, split the array again, the sub array now has it's own sub arrays
            childArrayTwo = mergeSort(childArrayTwo);
        }

        // Call the merge method, compares, sorts, and merges the parameterized arrays
        return merge(childArrayOne, childArrayTwo);
    }

    /**
     * Lexicographically compares each child array from the mergeSort method, and concatenates the
     * sub arrays in sorted lexicographically sorted order. Any element that remain in either array
     * then added to the end of the merged array. The merged array is finally returned, now properly
     * sorted.
     *
     * @param childArrayOne
     * @param childArrayTwo
     * @return
     */
    private static GymMember[] merge(GymMember[] childArrayOne, GymMember[] childArrayTwo) {

        /*
        The new sorted merged array will return; initialized to the
        length of both children arrays
         */
        GymMember[] mergedArray = new GymMember[childArrayOne.length + childArrayTwo.length];

        // Create a new variable representing the index of the merged array, at any time
        int mergedIndex = 0;

        // j represents index in childArrayOne, at any time
        int onesIndex = 0;

        // k represents index in childArrayTwo, at any time
        int twosIndex = 0;

        // While both sub arrays still contain elements
        while ((childArrayOne.length != onesIndex) && (childArrayTwo.length != twosIndex)) {

            /*
             Compare the values of both sub arrays at their relative index 0
             this is not the actual index zero, but a relative 0 to make sure we
             don't copy the same element, from the same sub array, into the merged array.
             Java's garbage collector will clean up
             */
            int comparisonValue = childArrayOne[onesIndex].compareTo(childArrayTwo[twosIndex]);

            // If the element in childArrayOne is greater than childArrayTwo
            if (comparisonValue > 0) {

                // Add the element of childArrayTWO to the next index of the merged array
                mergedArray[mergedIndex] = childArrayTwo[twosIndex];

                // Add one to the index of array two
                twosIndex++;

                // Add one to the index of the new merged array
                mergedIndex++;
            }

            /*
             The element in childArrayOne must by be less than or equal to the element of
             childArrayTwo
             */
            else {
                // Add the element of childArrayOne to the next index of the merged array
                mergedArray[mergedIndex] = childArrayOne[onesIndex];

                // Add one to the index of array one
                onesIndex++;

                // Add one to the index of the new merged array
                mergedIndex++;
            }
        }

        /*
        If one of the two arrays is empty, and the other still contains elements, the following
        while loops will execute
         */

        // While childArrayOne still has elements
        while (childArrayOne.length != onesIndex) {

            // Add the element to the next index in the new merged array
            mergedArray[mergedIndex] = childArrayOne[onesIndex];

            // Increment the merged array index by one
            mergedIndex++;

            // Increment the childArrayOne index by one
            onesIndex++;

        }

        // While childArrayTwo still has elements
        while (childArrayTwo.length != twosIndex) {

            // Add the element to the next index in the new merged array
            mergedArray[mergedIndex] = childArrayTwo[twosIndex];

            // Increment the merged array index by one
            mergedIndex++;

            // Increment the childArrayOne index by one
            twosIndex++;
        }

        // Return the lexicographically sorted and merged array
        return mergedArray;
    }

}
