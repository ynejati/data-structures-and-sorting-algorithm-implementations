import java.lang.reflect.Array;
import java.util.*;

/**
 * File: GraphUM.java
 * Author: Yousuf Nejati
 * Date: September 8th 2015
 * <p>
 * Purpose: Implement a custom directed graph using, at minimum, the Depth First Search method and the
 * Breadth First Search method. Must be able to store various data types, especially a String.
 * We will use a adjacencyMatrix to keep track of edges. We will use the object
 * oriented approach (uses more memory) instead of the the integer index approach (uses less memory)
 * <p>
 * Contains an internal node class. We will assume that only 0 or 1 connections can
 * exist between nodes at any given time.
 */
public class GraphUM<T> {

    private int initializedSize;
    private int currentIndex = 0;

    /*
    List of lists representing the adjacencyMatrix.
    Each index is a key representing if an edge exists between two nodes. A node's ID corresponds
    to an index value in the matrix.
    */
    public static boolean[][] adjMatrix;
    // List to keep track of nodes

    public NodeUMUC[] nodesList;
    public ArrayList<NodeUMUC> breadthSearch;
    public ArrayList<NodeUMUC> depthSearch;

    /**
     * Graph constructor
     * Se the maximum of nodes the graph can contain
     */
    public GraphUM(int initializedSize) {

        // Sets the class integer to the accepted integer
        this.initializedSize = initializedSize;

        // Initialize our matrix to size n x n
        adjMatrix = new boolean[initializedSize][initializedSize];

        // By default all of the boolean values in the matrix are set to false
        // Arrays.fill(adjMatrix, false);

        // Initializes our list of nodes to the accepted integer
        nodesList = new NodeUMUC[initializedSize];
    }


    /**
     * Adds a new node to the graph
     */
    public void addANode(T cargo) throws Exception {

        // Preform a check to make sure the graph is not full
        if (currentIndex == initializedSize) {
            throw new Exception("The graph is full. Remove a node, or restart.");
        } else {

            // Create a node id that corresponds with the size of our array of nodes and also corresponds
            // with the adjacency matrix. So that, if we add the first new node, the node's id will be 0
            int newNodeID = currentIndex;

            // Give this new node an ID and it's cargo
            NodeUMUC<T> newNode = new NodeUMUC<>(newNodeID, cargo);

            // Add the newNode to the node list, at the corresponding current index
            nodesList[currentIndex] = newNode;

            // Increment the current index by one
            currentIndex++;
        }

    }

    /**
     * Remove a node from the graph, when we remove a node, the value is null and the desire to
     * remove a node must be reflected in both the adjacency matrix and the list of nodes
     * <p>
     * In the adjacency matrix, removing a node means that for any connected nodes, we must
     * 1) find all of the connected nodes
     * 2) update all of the connected nodes by 'disconnecting' the removed node
     * 3) set all the values of the removed node within the node list to null
     */
    public void removeNode(int nodeID) {

        // Get the length of temporary array to store values
        int tempArrayLength = adjMatrix[nodeID].length;

        // Create a new array to temporarily store the integers representing the connected nodes
        int[] tempArray = new int[tempArrayLength];

        /*
        Update the adjacency matrix by finding
         */
        // Find all the connected nodes by iterating through the rows at index nodeID
        for (int i = 0; i < adjMatrix[nodeID].length; i++) {

            // For each index in the row, if the value is true,
            if (adjMatrix[nodeID][i] = true) {

                // Set the value to false
                adjMatrix[nodeID][i] = false;

                // Set the value at [i][nodeID] to null, therefore eliminating any connections
                adjMatrix[i][nodeID] = false;
            }
        }

        // Set the node in the node list to null
        nodesList[nodeID] = null;
    }


    /**
     * Add an edge between the two desired nodes
     */
    public void addEdge(int firstNodeID, int secondNodeID) {

        // At index firstNodeID, set the internal index (secondNodeID) to true
        adjMatrix[firstNodeID][secondNodeID] = true;

        // At index secondNodeID, set the internal index (firstNodeID) to true
        adjMatrix[secondNodeID][firstNodeID] = true;

    }


    /**
     * Remove the edge between two nodes
     */
    public void removeEdge(int firstNodeID, int secondNodeID) {

        // At index firstNodeID, set the internal index (secondNodeID) to false
        adjMatrix[firstNodeID][secondNodeID] = false;

        // At index secondNodeID, set the internal index (firstNodeID) to false
        adjMatrix[secondNodeID][firstNodeID] = false;
    }


    /**
     * Checks if an edge exists between the two desired nodes
     */
    public boolean hasEdge(int firstNodeID, int secondNodeID) {

        // Preform a check to see if an edge exists between two nodes

        // Check the boolean value of the first position
        boolean firstPosition = adjMatrix[firstNodeID][secondNodeID];

        // Check the boolean value of the second position
        boolean secondPosition = adjMatrix[secondNodeID][firstNodeID];

        // Assign the boolean value of the logical expression to local variable
        boolean evaluatedValue = (secondPosition && firstPosition);

        // If the edge exists, the return true, else return false, an edge does not exist.
        if (evaluatedValue) {
            return true;
        } else {
            return false;
        }
    }


    // ++++++++++++++++++++++++++++++++++++ begins DFS method +++++++++++++++++++++++++++++++++++++

    /**
     * Searches the graph by DFS
     * Uses a stack to handle nodes (LIFO)
     * <p>
     * Depth First Search: searches the nodes of a graph by following a chain of nodes until the
     * end. One we reach the end, we backtrack until we reach a node that is not at the end of
     * the chain. DFS is continued from this node.
     */
    public void depthFirstSearch(int initialNodeID) {


        // Check and make sure the visited value of each node within the graph is set to false
        for (NodeUMUC<T> n : nodesList) {
            n.visited = false;
        }

        NodeUMUC initialNode = nodesList[initialNodeID];

        // Create a stack to hold nodes to visit
        Stack<NodeUMUC<T>> tStack = new Stack<>();

        // Add the initial node to the stack
        tStack.push(initialNode);

        // Declare local node as the current node
        NodeUMUC currentNode;

        // While the stack is not empty
        while (!tStack.isEmpty()) {

            // Remove the current node from the top of the stack and preform checks
            currentNode = tStack.pop();

            // If the current node has not been visited, or if currentNode.visited == false
            if (currentNode.visited) {

                // Preform the visit operation which stores it in an array
                depthSearch.add(currentNode);

                // Then set it visited value to true
                currentNode.visited = true;

                /* Also, for each node connected to the parent (shares edges)
                 Searches the adjacency matrix to find connected nodes
                 */

                // Get the ID of the current node
                int currentNodesID = currentNode.getNodeID();


                // Check for any true values held at the index of the current node's ID in the
                // adjacency matrix. For each element found to be true...add them to the stack
                for (int i = 0; i < adjMatrix[currentNodesID].length; i++) {

                    boolean value = Array.getBoolean(adjMatrix[currentNodesID], i);

                    // If the value is true
                    if (value) {

                        // Set the value of connected node to the value of the node at index i
                        NodeUMUC<T> connectedNode = nodesList[i];

                        // Add the connected node to the top of the stack
                        tStack.push(connectedNode);
                    }
                }
            }
        }
    }
    // ++++++++++++++++++++ ends DFS method +++++++++++++++++++++++++++++++++++++++++++++++++++++


    // ++++++++++++++++++++ begins BFS method +++++++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * Searches the graph by BFS
     * Uses a queue to handle nodes (FIFO)
     * <p>
     * Breadth First Search: searches the nodes of  a graph by visiting all of the nodes within one
     * length from the initial node in the graph. Next, we visit all the nodes at a distance of two
     * lengths from the initial node, then three, ..etc, until all of the nodes have been visited.
     */
    public void breadthFirstSearch(int initialNodeID) {


        // Check and make sure the visited value of each node within the graph is set to false
        for (NodeUMUC n : nodesList) {
            n.visited = false;
        }

        NodeUMUC initialNode = nodesList[initialNodeID];

        // Create a queue to hold nodes to visit
        Queue<NodeUMUC<T>> tQueue = new LinkedList<>();

        // Add the initial node to the queue
        tQueue.add(initialNode);

        // While the queue is not empty
        while (!tQueue.isEmpty()) {

            // Remove the current node from the que and preform checks
            NodeUMUC<T> currentNode = tQueue.remove();

            // If the current node has not been visited, or if currentNode.visited == false
            if (currentNode.visited) {

                // Preform the visit operation which stores it in an array
                breadthSearch.add(currentNode);

                // Then set it visited value to true
                currentNode.visited = true;

                /* Also, for each node connected to the parent (shares edges)
                 Searches the adjacency matrix to find connected nodes
                 */

                // Get the ID of the current node
                int currentNodesID = currentNode.getNodeID();


                // Check for any true values held at the index of the current node's ID in the
                // adjacency matrix. For each element found to be true...add them to the queue
                for (int i = 0; i < adjMatrix[currentNodesID].length; i++) {

                    // Get the boolean value of the current index in the array
                    boolean value = Array.getBoolean(adjMatrix[currentNodesID], i);

                    // If the value is true
                    if (value) {

                        // Set the value of connected node to the value of the node at index i
                        NodeUMUC<T> connectedNode = nodesList[i];

                        // Add the connected node to the queue
                        tQueue.add(connectedNode);
                    }
                }
            }
        }
    }
    // +++++++++++++++++++++++++++++++++++++ ends BFS method +++++++++++++++++++++++++++++++++++++


    /**
     * Returns all the nodes within one degree of the current node
     */
    public String getAdjacentNodes(int nodeID) {

        List<Integer> adjacentNodes = new ArrayList<>();

        // Get the ID of the desired node
        // int nodeID = thisNode.getNodeID();

        // For this node, run down the matrix and get the index of all the values that are true
        for (int i = 0; i < adjMatrix[nodeID].length; i++) {

            // Get the boolean value of the current index in the array
            boolean value = Array.getBoolean(adjMatrix[nodeID], i);

            // If the value is true
            if (value) {

                // Store this 'nodes' ID in a new array
                adjacentNodes.add(i);
            }
        }
        // Return the array of node ID's
        return adjacentNodes.toString();
    }

    /**
     * Prints a list of nodes and their cargo
     *
     * @return
     */
    public String toString() {

        // Prints out the node list which contains, node ID, its cargo, and any adjacent nodes
        return Arrays.toString(nodesList);
    }



    // +++++++++++++++++++++++++++++++ Internal node class BEGIN ++++++++++++++++++++++++++++++++++


    /**
     * Node class
     *
     * @param <T>
     */
    class NodeUMUC<T> {


        // Every node must have an ID
        // The ID corresponds to the next index in the adjacency list
        int nodeID;

        // Every node contains some cargo
        T cargo;

        // Every node has 0 to 1 edge
        int edge = 0;

        // Indicates if the node has been visited
        boolean visited = false;

        /**
         * Node constructor
         */
        public NodeUMUC(int nodeID, T cargo) {
            this.cargo = cargo;
            this.nodeID = nodeID;
        }

        public int getEdge() {
            return edge;
        }

        public void setEdge(int edge) {
            this.edge = edge;
        }

        public int getNodeID() {
            return nodeID;
        }

        public void setNodeID(int nodeID) {
            this.nodeID = nodeID;
        }

        public String toString() {
            return " Node ID: " + nodeID + ", Cargo: " + cargo + ", Adjacent nodes: "
                + getAdjacentNodes(nodeID);
        }
    }
    //  +++++++++++++++++++++++++++++++ Internal node class END +++++++++++++++++++++++++++++++++++

}

