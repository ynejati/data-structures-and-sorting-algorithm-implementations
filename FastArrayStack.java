import java.util.Arrays;

/**
 * File: FastArrayStack.java
 * Author: Yousuf Nejati
 * Date: August 30th 2015
 *
 * Purpose: Implementation of a custom FastArrayStack,
 * utilizing library functions for copying and moving blocks of data.
 * Doing so can speed up results by a factor of 2 and 3, depending on the types
 * of operations performed.
 * <p>
 * built in arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
 * Copies an array from the specified source array, beginning at the specified
 * position, to the specified position of the destination array.
 */

public class FastArrayStack<T> {

    /**
     * Instance variables for the class
     */
    private T[] objectArray;
    protected int data;
    private int n;

    /**
     * Default class constructor, initializes the stack to 10
     */
    public FastArrayStack() {
        final int INITIAL_CAPACITY = 10;
        data = 0;
        objectArray = (T[]) new Object[INITIAL_CAPACITY];
    }

    /**
     * Class constructor that accepts a integer value and
     * initializes the array to accepted value
     *
     * @param newInitialCapacity
     */
    public FastArrayStack(int newInitialCapacity) {
        if (newInitialCapacity < 0) {
            throw new IllegalArgumentException();
        }
        data = 0;
        objectArray = (T[]) new Object[newInitialCapacity];
    }

    /**
     * Method to double the size of an array under certain conditions
     */
    void resize() {
        T[] b = (T[]) new Object[(Math.max(2 * n, 1))];
        System.arraycopy(objectArray, 0, b, 0, n);
        objectArray = b;
    }

    /**
     * Method to add an object to the stack at a particular index
     *
     * @param i
     * @param x
     */
    void add(int i, T x) {
        if (i < 0 || i > n) {
            throw new IndexOutOfBoundsException();
        }
        if (n + 1 > objectArray.length) {
            resize();
        }
        System.arraycopy(objectArray, i, objectArray, i + 1, n - i);
        objectArray[i] = x;
        n++;
    }

    /**
     * Method to remove an object from the stack at a particular index
     *
     * @param i
     * @return
     */
    T remove(int i) {
        if (i < 0 || i > n - 1)
            throw new IndexOutOfBoundsException();
        T x = objectArray[i];
        System.arraycopy(objectArray, i + 1, objectArray, i, n - i - 1);
        n--;
        if (objectArray.length >= 3 * n)
            resize();
        return x;
    }

    /**
     * Produces a string representation of our class
     */
    public String toString() {
        return Arrays.deepToString(objectArray);
    }
}



