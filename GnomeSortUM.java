/**
 * File: GnomeSortUM.java
 * Author: Yousuf Nejati
 * Date: September 21 2015
 * Purpose: Implement a sorting algorithm that takes n^2 time, sorting using default lexicographical
 * order
 */
public class GnomeSortUM<T extends Comparable> {

    // gnomeSort or "stupid sort"
    public void gnomeSort(T array[]) {
        //
        int position = 1;
        int arrayLength = array.length;

        while (position < arrayLength) {

            // Compare the current element to the previous element
            // Return -1 if current element is less, 0 if equal, and 1 if greater
            int currentComparisonValue = array[position].compareTo(array[position - 1]);

            // If the value of the current element is greater or equal to the previous element
            if (currentComparisonValue >= 0) {

                // increment the position by one, move to the next position in the array
                position += 1;
            }
            // If the value of the current element is less than the previous element
            else {

                // Swap the previous element with the current element
                T tempValue = array[position];
                array[position] = array[position - 1];
                array[position - 1] = tempValue;

                // if the current position is greater than one, subtract one and start again
                if (position > 1) {
                    position -= 1;
                }
            }
        }
    }
}
