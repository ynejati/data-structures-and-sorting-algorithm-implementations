import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * File: StackUM.java
 * Author: Yousuf Nejati
 * Date: August 30th 2015
 * Purpose: Implement our own stack to store instances of type T, and create
 * custom methods standard to a stack data structure
 */
public class StackUM<T> {

    private int stackSize = 0;
    private Object theStack[];
    private int top;

    /**
     * Class constructor creates class instances accepting an integer as an argument
     * to initialize  the desired size of our stack
     */
    StackUM(int stackSize) {
        this.stackSize = stackSize;
        theStack = new Object[stackSize];
        top = -1;
    }

    /**
     * Pushes an item into the top of the stack
     * Checks to see if the size of the stack is full, and if so doubles the size of the stack
     * @param object
     */
    public void push(T object) throws EmptyStackException {
        if (top + 1 >= stackSize) {
            // double the array
            T[] newArray = (T[]) new Object[(stackSize * 2)];
            for (int i = 0; i < newArray.length; i++) {
            }
            System.arraycopy(theStack, 0, newArray, 0, stackSize);
        }
        theStack[++top] = object;

    }

    /**
     * Removes the object at the top of the stack and returns that object
     * as the value of this function
     *
     * @return
     */
    public T pop() {
        if (stackSize == 0) {
            throw new EmptyStackException();
        }
        T object = (T) theStack[--stackSize];
        theStack[stackSize] = null + ", Index " + (stackSize + 1) + " is empty! \n";
        return object;
    }

    /**
     * Looks a the object at the top of this stack without removing  it from the stack
     *
     * @return
     */
    public T peek() {
        if (stackSize == 0) {
            throw new EmptyStackException();
        }
        T object = (T) theStack[stackSize - 1];
        return object;
    }

    /**
     * Produces a string representation of our class
     */
    @Override public String toString() {
        return Arrays.deepToString(theStack);
    }
}
