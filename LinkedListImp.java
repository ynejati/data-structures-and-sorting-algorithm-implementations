

/**
 * File: LinkedListImp.java
 * Author: Yousuf Nejati
 * Date: September 2nd 2015
 * Purpose: Implement a custom linked list class with the standard specified methods, to be used
 * with GUI and GymMember objects
 */
public class LinkedListImp<T> {

    // Node, the head of the linked list
    private NodeUMUC<T> head;
    // Node, the tail of the linked list
    private NodeUMUC<T> tail;

    // Length of linked list
    private int length;

    // Default class constructor sets the head to null
    LinkedListImp() {
        head = null;
        tail = null;
        length = 0;
    }

    /**
     * Add node, beginning of the list
     *
     * @param cargo
     */
    void insertHead(T cargo) {
        NodeUMUC<T> newHead = new NodeUMUC<>(cargo);
        if (head != null) {
            newHead.setNext(head);
            head.setPrev(newHead);
        } else {
            tail = newHead;
        }
        head = newHead;
        length++;
    }

    /**
     * Add node, end of list
     *
     * @param nuNode
     */
    void insertTail(T nuNode) {
        NodeUMUC<T> newTail = new NodeUMUC<>(nuNode);
        if (tail != null) {
            newTail.setPrev(tail);
            tail.setNext(newTail);
        } else {
            head = newTail;
        }
        tail = newTail;
        length++;
    }

    /**
     * REMOVE node, AT INDEX
     * and connect the list
     *
     * @param index
     * @return
     */
    T removeElementAt(int index) {
        if (length < 1 || index < 0 || index >= length) {
            return null;
        }
        if (index == 0) {
            return removeHead();
        }
        if (index == length - 1) {
            return removeTail();
        }

        // somewhere in the middle
        NodeUMUC<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.getNext();
        }
        NodeUMUC<T> prevNode = node.getPrev();
        NodeUMUC<T> nextNode = node.getNext();

        prevNode.setNext(nextNode);
        nextNode.setPrev(prevNode);

        length--;
        return node.getCargo();
    }


    /**
     * Remove node, end of list
     *
     * @return
     */
    T removeTail() {
        if (tail == null) {
            return null;
        }
        NodeUMUC<T> oldTail = tail;
        tail = oldTail.getPrev();
        if (tail == null) {
            head = null;
        }
        length--;
        return oldTail.getCargo();
    }

    /**
     * Remove node, beginning of list
     */
    T removeHead() {
        if (head == null) {
            return null;
        }
        NodeUMUC<T> oldHead = head;
        head = oldHead.getNext();
        if (head == null) {
            tail = null;
        }
        length--;
        return oldHead.getCargo();
    }

    /**
     * Display node, at index
     */
    T peekElementAt(int index) {
        if (length < 1 || index < 0 || index >= length) {
            return null;
        }
        if (index == 0) {
            return peekHead();
        }
        if (index == length - 1) {
            return peekTail();
        }

        // somewhere in the middle
        NodeUMUC<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.getNext();
        }
        return node.getCargo();
    }

    /**
     * Display node, at beginning
     */
    T peekHead() {
        if (head == null) {
            return null;
        }
        return head.getCargo();
    }

    /**
     * Display node, at end
     *
     * @return
     */
    T peekTail() {
        if (tail == null) {
            return null;
        }
        return tail.getCargo();
    }


    int getLength() {
        return length;
    }


    /**
     * @return
     */
    @Override public String toString() {
        return "LinkedListImp";
    }

    /**
     * Define a node to be used in our 'LinkedListImp' class, which represents a singly
     * linked list, that will be used alongside the UI and 'Gym_Member' class we created for
     * homework 1.
     */
    private class NodeUMUC<T> {

        private NodeUMUC<T> next;
        private NodeUMUC<T> prev;
        private T cargo;

        // The node class constructor, each node holds an object or "cargo" and a reference to the next node
        public NodeUMUC(T cargo) {
            this.cargo = cargo;
        }

        /**
         * Get nodes contents
         *
         * @return cargo
         */
        private T getCargo() {
            return cargo;
        }

        /**
         * Get the next node
         *
         * @return next
         */
        private NodeUMUC<T> getNext() {
            return next;
        }

        /**
         * Assign the next node
         *
         * @param next
         */
        public void setNext(NodeUMUC<T> next) {
            this.next = next;
        }

        /**
         * Get the previous node
         *
         * @return prev
         */
        public NodeUMUC<T> getPrev() {
            return prev;
        }

        /**
         * Set the previous node
         *
         * @param prev
         */
        public void setPrev(NodeUMUC<T> prev) {
            this.prev = prev;
        }

        /**
         * To string method to return cargo field
         * built by IDE
         *
         * @return
         */
        @Override public String toString() {
            return "NodeUMUC{" +
                "cargo=" + String.valueOf(cargo) +
                '}';
        }
    }
}

